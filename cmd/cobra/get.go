package cobra

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/valdemar-ceccon/secret-cli"
)

var getCmd = &cobra.Command{
	Use:   "get",
	Short: "Gets as secret in you secret storage",
	Run: func(cmd *cobra.Command, args []string) {
		v := secret.File(encodingKey, secretsPath())
		key := args[0]
		value, err := v.Get(key)
		if err != nil {
			panic(err)
		}
		fmt.Printf("%s=%s\n", key, value)
	},
}

func init() {
	RootCmd.AddCommand(getCmd)
}
