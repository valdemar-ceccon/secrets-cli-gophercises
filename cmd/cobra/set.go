package cobra

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/valdemar-ceccon/secret-cli"
)

var setCmd = &cobra.Command{
	Use:   "set",
	Short: "Sets as secret in you secret storage",
	Run: func(cmd *cobra.Command, args []string) {
		v := secret.File(encodingKey, secretsPath())
		key, value := args[0], args[1]
		err := v.Set(key, value)
		if err != nil {
			panic(err)
		}
		fmt.Println("Value stored")
	},
}

func init() {
	RootCmd.AddCommand(setCmd)
}
