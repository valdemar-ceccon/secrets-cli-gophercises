package main

import "gitlab.com/valdemar-ceccon/secret-cli/cmd/cobra"

func main() {
	cobra.RootCmd.Execute()
}
